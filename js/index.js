$(document).ready(function(){

  	minas = 0;
  	filas = 0;
  	columnas = 0;

  	nivelDificultad(1);

  	//controla si el mapa se genera antes o despues del primer click
  	//true = el mapa se genera despues del primer click
  	//false = el mapa se genera antes del primer click
  	inicioSeguro=true;

  	intervalSegundos = 0;
  	intervaloMinutos = 0;
  	
  	iniciarPartida();	

  	$("#nueva_partida").on("click", function(){
  		reiniciarPartida();
  	});

  	//Control del inicio seguro
  	$(".inicio-seguro").on("click", function(){
  		if(inicioSeguro){
  			inicioSeguro=false;
  			$(this).css("background", "white");
  			$(this).attr("title", "Inicio seguro desactivado");
  			$(".marcador-inicio-seguro").text("(Desactivado)");
  		}else{
  			inicioSeguro=true;
  			$(".inicio-seguro").css("background", "#BEE4FA");
  			$(".inicio-seguro").attr("title", "Inicio seguro activado");
  			$(".marcador-inicio-seguro").text("(Activado)");
  		} 
  		reiniciarPartida();
  	});

  	$(".inicio-seguro, .marcador-inicio-seguro").hover(function(){
  		$(".inicio-seguro").css("background", "#DBECF5");
  	});

  	$(".inicio-seguro, .marcador-inicio-seguro").mouseout(function(){
  		if(inicioSeguro){
  			$(".inicio-seguro").css("background", "#BEE4FA");
  		}else{
  			$(".inicio-seguro").css("background", "white");
  		} 
  	});

  	//niveles de dificultad
  	$("#facil").on("click", function(){
  		nivelDificultad(1);
  		reiniciarPartida();
  	});

  	$("#medio").on("click", function(){
  		nivelDificultad(2);
  		reiniciarPartida();
  	});

  	$("#dificil").on("click", function(){
  		nivelDificultad(3);
  		reiniciarPartida();
  	});

  	$("#experto").on("click", function(){
  		nivelDificultad(4);
  		reiniciarPartida();
  	});
});

function iniciarPartida(){

  	matriz = [];
  	matrizDescubierta = [];
  	matrizMinasMarcadas = [];

  	numeroDescubiertos = 0;
  	numeroMinasMarcadas = 0;
  	segundos=1;
  	minutos=0;

  	gameStoped=false;
  	win=false;
  	primerClick = false; //True es que ya esta creado, false que el mapa no se ha creado

  	if(!inicioSeguro){
  		generarMapa(0,0);
  		generarTabla();
  	}else{
  		generarTabla();
  	}

  	marcadorMinas();
  	expresionEmoticono("happy");
  	
  	//La separacion de la "s" del resto de la palabra es debido a que el letter-spacing
  	//genera un espacio de mas a la derecha, asi ocasionando que no se pueda centrar
  	$("header>h1").html("<span>Buscamina</span>s"); 
  	$("header").removeAttr("class");

  	// evito que se ejecute el evento por defecto del click derecho
  	$('div').on('contextmenu', function(e) {
		e.preventDefault();
	});

  	$("td").mousedown(function(e){
  		if(e.which == 3){
  			if(!gameStoped) marcarMina(e.currentTarget.id);
  		}else{
  			play(e.currentTarget.id);
  		}
  	});

}

// Genera la tabla html una vez ya tiene el numero de filas y columnas.
function generarTabla(){

	var tabla = "";

	//Si el modo de inicio es seguro (true), creara las matrices.
	if(inicioSeguro){
		for(var i=0; i<filas; i++) {
	    	matriz[i] = new Array(columnas-1);
	    	matrizDescubierta[i] = new Array(columnas-1);
	    	matrizMinasMarcadas[i] = new Array(columnas-1);
	    	matrizMinasMarcadas[i][x]=false;
  			matrizDescubierta[i][x]=false;
		}
	}

	for(var i=0; i<filas; i++){
  		tabla += "<tr>";
  		for(var x=0; x<columnas; x++){
  			if(matriz[i][x]==0){
  				matriz[i][x]="";
  			}
  			tabla += "<td class='text-center' id='i" + i + "x" + x + "'></td>";
  		}
  		tabla += "</tr>";
  	}

  	$(".tabla").html(tabla);
}

//Genera las minas y calcula el numero de minas que tiene alrededor cada casilla
function generarMapa(val1, val2){

	var num_minas = 0;

	if(!inicioSeguro){
		for(var i=0; i<filas; i++) {
	    	matriz[i] = new Array(columnas-1);
	    	matrizDescubierta[i] = new Array(columnas-1);
	    	matrizMinasMarcadas[i] = new Array(columnas-1);
	    	matrizMinasMarcadas[i][x]=false;
  			matrizDescubierta[i][x]=false;
		}
	}
		
	//colocacion de minas
	while(num_minas<minas){

		var ran1 = getRandom(0,filas-1);
		var ran2 = getRandom(0,columnas-1);

		//Si el inicio es seguro, no colocará ninguna mina en la casilla clicada ni en las 8 de su alrededor
		if(inicioSeguro){
			if((ran1!=val1 || ran2!=val2) && (ran1!=val1 || ran2!=val2-1)&& (ran1!=val1 || ran2!=parseInt(val2)+1) && (ran1!=val1-1 || ran2!=val2) && 
				(ran1!=val1-1 || ran2!=val2-1) && (ran1!=val1-1 || ran2!=parseInt(val2)+1) && (ran1!=parseInt(val1)+1 || ran2!=val2-1) && 
				(ran1!=parseInt(val1)+1 || ran2!=val2) && (ran1!=parseInt(val1)+1 || ran2!=parseInt(val2)+1)){
				
				if(matriz[ran1][ran2]!="X"){
					matriz[ran1][ran2]="X";
					num_minas++;
				}
			}
		}else{
			if(matriz[ran1][ran2]!="X"){
				matriz[ran1][ran2]="X";
				num_minas++;
			}
		}
	}
	
	//calcula el numero de minas que hay alrededor de cada una de las celdas
	for(var i=0; i<filas; i++){
  		for(var x=0; x<columnas; x++){

  			if(matriz[i][x]!="X"){

  				var cantidad_minas = 0;

  				if(i>0){
  					if(x>0){
  						if(matriz[i-1][x-1]=="X"){
		  					cantidad_minas++;
		  				}
  					}
  					
		  			if(matriz[i-1][x]=="X"){
		  				cantidad_minas++;
		  			}

		  			if(x<columnas-1){
		  				if(matriz[i-1][x+1]=="X"){
		  					cantidad_minas++;
		  				}
		  			}			
		  		}

		  		if(x>0){
		  			if(matriz[i][x-1]=="X"){
		  				cantidad_minas++;
			  		}
		  		}
		  			
		  		if(x<columnas-1){
		  			if(matriz[i][x+1]=="X"){
		  				cantidad_minas++;
		  			}
		  		}
		  			
		  		if(i<filas-1){
		  			if(x>0){
		  				if(matriz[i+1][x-1]=="X"){
			  				cantidad_minas++;
				  		}
		  			}
		  				
			  		if(matriz[i+1][x]=="X"){
			  			cantidad_minas++;
			  		}

			  		if(x<columnas-1){
			  			if(matriz[i+1][x+1]=="X"){
				  			cantidad_minas++;
				  		}
			  		}	
		  		}

	  			matriz[i][x]=cantidad_minas;

  			}
  		}
  	}	
}

//Función que controla el juego en cada click del jugador
function play(id){
	var i = parseInt(id.substring(id.indexOf("i") + 1, id.indexOf("x")));
	var x = parseInt(id.substring(id.indexOf("x") + 1));

	if(!primerClick){
		primerClick=true;
		if(inicioSeguro) generarMapa(i,x);
		iniciarMarcadorTiempo();
	}
	
	//comprueba donde, el que, y que debe de hacer el juego segun la casilla que haya clicado el jugador
	if(!matrizDescubierta[i][x] && !matrizMinasMarcadas[i][x]){
		if(matriz[i][x]!="X"){

			if(matriz[i][x]!=0){
				matrizDescubierta[i][x]=true;
				voltearCasillas();
			}else{
				despejarCasillas(i,x);
				matrizDescubierta[i][x]=true;
				voltearCasillas();
			}
		}else{
			gameOver(i,x);
		}
	}else if(matrizDescubierta[i][x]){

		if(comprobarMinasMarcadas(i,x)){
			despejarCasillas(i,x);
			voltearCasillas();
		}

	}
}

//Funcion encargada en comprobar si la casilla puede ser volteada
function despejarCasillas(valI,valX){

	var recorrido = 1;
	
	if(valI>0){
		if(valX>0){
			//recorre arriba a la izquierda
			while((matriz[valI-recorrido][valX-recorrido]>=0 && !matrizDescubierta[valI-recorrido][valX-recorrido]) || matriz[valI-recorrido][valX-recorrido]=="X"){
				if(matriz[valI-recorrido][valX-recorrido]!="X" && !matrizMinasMarcadas[valI-recorrido][valX-recorrido]){
					matrizDescubierta[valI-recorrido][valX-recorrido]=true;
				}
				
				if(matriz[valI-recorrido][valX-recorrido]==0 && !matrizMinasMarcadas[valI-recorrido][valX-recorrido]){
					despejarCasillas(valI-recorrido, valX-recorrido);
					recorrido++;
					if(valI-recorrido<0 || valX-recorrido<0){
						break;
					}
				}else{
					if(matriz[valI-recorrido][valX-recorrido]=="X" && !matrizMinasMarcadas[valI-recorrido][valX-recorrido]){
						gameOver(valI-recorrido, valX-recorrido);
						break;
					}else{
						break;
					}
				}

				
			}
			recorrido=1;
		}


		//recorre hacia arriba
		while((matriz[valI-recorrido][valX]>=0 && !matrizDescubierta[valI-recorrido][valX]) || matriz[valI-recorrido][valX]=="X"){
			if(matriz[valI-recorrido][valX]!="X" && !matrizMinasMarcadas[valI-recorrido][valX]){
				matrizDescubierta[valI-recorrido][valX]=true;
			}
			
			if(matriz[valI-recorrido][valX]==0 && !matrizMinasMarcadas[valI-recorrido][valX]){
				despejarCasillas(valI-recorrido, valX);
				recorrido++;
				if(valI-recorrido<0){
					break;
				}
			}else{
				if(matriz[valI-recorrido][valX]=="X" && !matrizMinasMarcadas[valI-recorrido][valX]){
					gameOver(valI-recorrido, valX);
					break;
				}else{
					break;
				}
			}

			
		}
		recorrido=1;

		if(valX<columnas-1){
			//recorre arriba a la derecha
			while((matriz[valI-recorrido][valX+recorrido]>=0 && !matrizDescubierta[valI-recorrido][valX+recorrido]) || matriz[valI-recorrido][valX+recorrido]=="X"){
				if(matriz[valI-recorrido][valX+recorrido]!="X" && !matrizMinasMarcadas[valI-recorrido][valX+recorrido]){
					matrizDescubierta[valI-recorrido][valX+recorrido]=true;
				}
				
				if(matriz[valI-recorrido][valX+recorrido]==0 && !matrizMinasMarcadas[valI-recorrido][valX+recorrido]){
					despejarCasillas(valI-recorrido, valX+recorrido);
					recorrido++;
					if(valI-recorrido<0 || valX+recorrido>columnas-1){
						break;
					}
				}else{
					if(matriz[valI-recorrido][valX+recorrido]=="X" && !matrizMinasMarcadas[valI-recorrido][valX+recorrido]){
						gameOver(valI-recorrido, valX+recorrido);
						break;
					}else{
						break;
					}
				}

				
			}
			recorrido=1;
		}
		
	}

	if(valX>0){
		//recorre a la izquierda
		while((matriz[valI][valX-recorrido]>=0 && !matrizDescubierta[valI][valX-recorrido]) || matriz[valI][valX-recorrido]=="X"){
			if(matriz[valI][valX-recorrido]!="X" && !matrizMinasMarcadas[valI][valX-recorrido]){
				matrizDescubierta[valI][valX-recorrido]=true;
			}
			
			if(matriz[valI][valX-recorrido]==0 && !matrizMinasMarcadas[valI][valX-recorrido]){
				despejarCasillas(valI, valX-recorrido);
				recorrido++;
				if(valX-recorrido<0){
					break;
				}
			}else{
				if(matriz[valI][valX-recorrido]=="X" && !matrizMinasMarcadas[valI][valX-recorrido]){
					gameOver(valI, valX-recorrido);
					break;
				}else{
					break;
				}
			}

			
		}
		recorrido=1;
	}
	
	if(valX<columnas-1){
		//recorre a la derecha
		while((matriz[valI][valX+recorrido]>=0 && !matrizDescubierta[valI][valX+recorrido]) || matriz[valI][valX+recorrido]=="X"){
			if(matriz[valI][valX+recorrido]!="X" && !matrizMinasMarcadas[valI][valX+recorrido]){
				matrizDescubierta[valI][valX+recorrido]=true;
			}
			
			if(matriz[valI][valX+recorrido]==0 && !matrizMinasMarcadas[valI][valX+recorrido]){
				despejarCasillas(valI, valX+recorrido);
				recorrido++;
				if(valX+recorrido>columnas-1){
					break;
				}
			}else{
				if(matriz[valI][valX+recorrido]=="X" && !matrizMinasMarcadas[valI][valX+recorrido]){
					gameOver(valI, valX-recorrido);
					break;
				}else{
					break;
				}
			}

			
		}
		recorrido=1;
	}

	if(valI<filas-1){
		
		if(valX>0){
			//recorre abajo a la izquierda
			while((matriz[valI+recorrido][valX-recorrido]>=0 && !matrizDescubierta[valI+recorrido][valX-recorrido]) || matriz[valI+recorrido][valX-recorrido]=="X"){
				if(matriz[valI+recorrido][valX-recorrido]!="X" && !matrizMinasMarcadas[valI+recorrido][valX-recorrido]){
					matrizDescubierta[valI+recorrido][valX-recorrido]=true;
				}
				
				if(matriz[valI+recorrido][valX-recorrido]==0 && !matrizMinasMarcadas[valI+recorrido][valX-recorrido]){
					despejarCasillas(valI+recorrido, valX-recorrido);
					recorrido++;
					if(valI+recorrido>filas-1 || valX-recorrido<0){
						break;
					}
				}else{
					if(matriz[valI+recorrido][valX-recorrido]=="X" && !matrizMinasMarcadas[valI+recorrido][valX-recorrido]){
						gameOver(valI+recorrido, valX-recorrido);
						break;
					}else{
						break;
					}
				}

				
			}
			recorrido=1;
		}
		
		//recorre abajo
		while((matriz[valI+recorrido][valX]>=0 && !matrizDescubierta[valI+recorrido][valX]) || matriz[valI+recorrido][valX]=="X"){
			if(matriz[valI+recorrido][valX]!="X" && !matrizMinasMarcadas[valI+recorrido][valX]){
				matrizDescubierta[valI+recorrido][valX]=true;
			}
			
			if(matriz[valI+recorrido][valX]==0 && !matrizMinasMarcadas[valI+recorrido][valX]){
				despejarCasillas(valI+recorrido, valX);
				recorrido++;
				if(valI+recorrido>filas-1){
					break;
				}
			}else{
				if(matriz[valI+recorrido][valX]=="X" && !matrizMinasMarcadas[valI+recorrido][valX]){
					gameOver(valI+recorrido, valX);
					break;
				}else{
					break;
				}
			}

			
		}
		recorrido=1;

		if(valX<columnas-1){
			//recorre abajo a la derecha
			while((matriz[valI+recorrido][valX+recorrido]>=0 && !matrizDescubierta[valI+recorrido][valX+recorrido]) || matriz[valI+recorrido][valX+recorrido]=="X"){
				if(matriz[valI+recorrido][valX+recorrido]!="X" && !matrizMinasMarcadas[valI+recorrido][valX+recorrido]){
					matrizDescubierta[valI+recorrido][valX+recorrido]=true;
				}
				
				if(matriz[valI+recorrido][valX+recorrido]==0 && !matrizMinasMarcadas[valI+recorrido][valX+recorrido]){
					despejarCasillas(valI+recorrido, valX+recorrido);
					recorrido++;
					if(valI+recorrido>filas-1 ||valX+recorrido>columnas-1){
						break;
					}
				}else{
					if(matriz[valI+recorrido][valX+recorrido]=="X" && !matrizMinasMarcadas[valI+recorrido][valX+recorrido]){
						gameOver(valI+recorrido, valX+recorrido);
						break;
					}else{
						break;
					}
				}

				
			}
			recorrido=1;
		}

	}

}

//Esta funcion se encarga de dar la vuelta a todas las casillas donde "matrizDescubierta" sea true.
function voltearCasillas(){

	var cuenta = 0;

	for(var i=0; i<filas; i++){
  		for(var x=0; x<columnas; x++){
  			if(matrizDescubierta[i][x]){
  				cuenta++;
  				if(matriz[i][x]!="X"){
  					$("#i"+i+"x"+x).css("background", "none");
  					if(matriz[i][x]!=0){
  						$("#i"+i+"x"+x).text(matriz[i][x]);
  						setColor("#i"+i+"x"+x);
  					}	
  				}
  			}	
  		}
  	}

  	numeroDescubiertos=cuenta;

  	//Si todas las casillas han sido volteadas, la partida se dará por finalizada y ganada
  	if(numeroDescubiertos==(filas*columnas)-minas && !gameStoped){
  		victoria();
	}
}

//Esta funcion se encarga marcar y desmarcar las minas, comprueba todas las posibilidades que pueden darse.
function marcarMina(id){

	var i = parseInt(id.substring(id.indexOf("i") + 1, id.indexOf("x")));
	var x = parseInt(id.substring(id.indexOf("x") + 1));

	if(!matrizDescubierta[i][x]){

		if(matrizMinasMarcadas[i][x]){
			matrizMinasMarcadas[i][x] = false;
			numeroMinasMarcadas--;
			$("#i"+i+"x"+x).text("");
		}else if((minas-numeroMinasMarcadas>0 && !gameStoped) || win){
			matrizMinasMarcadas[i][x] = true;
			numeroMinasMarcadas++;
			$("#i"+i+"x"+x).html("<img src='images/banderita.png' class='obj-tablero banderitas'/>");
		}else if(gameStoped){
			matrizMinasMarcadas[i][x] = true;
			numeroMinasMarcadas++;
			$("#i"+i+"x"+x).html("<img src='images/minas.png' class='obj-tablero minas'/>");
		}

		marcadorMinas();
			
	}
}

//Esta funcion comprueba si se han marcado el mismo o superior numero de minas alrededor de la casilla que ha marcado el jugador
//En caso de que sea así la funcion devolverá true, en caso contrario devolverá false
function comprobarMinasMarcadas(valI, valX){

	var cantidad_total = matriz[valI][valX];
	var cantidad = 0;
	var marcadas = false;

	if(valI>0){
		if(valX>0){
			if(matrizMinasMarcadas[valI-1][valX-1] && !matrizDescubierta[valI-1][valX-1]){
				cantidad++;
			}
		}
		
		if(matrizMinasMarcadas[valI-1][valX] && !matrizDescubierta[valI-1][valX]){
			cantidad++;
		}

		if(valX<columnas-1){
			if(matrizMinasMarcadas[valI-1][valX+1] && !matrizDescubierta[valI-1][valX+1]){
				cantidad++;
			}
		}
		
	}

	if(valX>0){
		if(matrizMinasMarcadas[valI][valX-1] && !matrizDescubierta[valI][valX-1]){
			cantidad++;
		}
	}
	
	if(valX<columnas-1){

		if(matrizMinasMarcadas[valI][valX+1] && !matrizDescubierta[valI][valX+1]){
			cantidad++;
		}
	}

	if(valI<filas-1){
		
		if(valX>0){
			if(matrizMinasMarcadas[valI+1][valX-1] && !matrizDescubierta[valI+1][valX-1]){
				cantidad++;
			}
		}

		if(matrizMinasMarcadas[valI+1][valX] && !matrizDescubierta[valI+1][valX]){
				cantidad++;
		}

		if(valX<columnas-1){

			if(matrizMinasMarcadas[valI+1][valX+1] && !matrizDescubierta[valI+1][valX+1]){
				cantidad++;
			}

		}
	}

	if(cantidad>=cantidad_total){
		marcadas=true;
	}
	
	return marcadas;

}

//Esta funcion se ejecutará cuando el juego finalice, se haya ganado o perdido
function gameOver(valI, valX){

	gameStoped=true;

	clearInterval(intervalSegundos);
	clearInterval(intervalMinutos);

	//recorre cada casilla comprobando las distintas posibilidades
	for(var i=0; i<filas; i++){
  		for(var x=0; x<columnas; x++){
  			if(!matrizDescubierta[i][x] && matriz[i][x]!="X" && !matrizMinasMarcadas[i][x]){
  				matrizDescubierta[i][x]=true;
  			}else if(!matrizDescubierta[i][x] && !matrizMinasMarcadas[i][x]){
  				marcarMina("i"+i+"x"+x);
  			}else if(!matrizDescubierta[i][x] && matrizMinasMarcadas[i][x] && matriz[i][x]!="X"){
  				$("#i"+i+"x"+x).html("<img src='images/error_banderita.png' class='obj-tablero error-banderitas'/>");
  			}
  		}
  	}

  	//Si el valor de valI es mayor o igual a 0, significa que el jugador ha perdido
  	if(valI>=0){
  		derrota(valI, valX);
  	}
  	
}

//Controla el marcador de minas
function marcadorMinas(){

	var num_minas = minas-numeroMinasMarcadas;

	if(!gameStoped){
		
		if(num_minas<10) $("#minas").text("00"+num_minas);
			
		else if(minas<100) $("#minas").text("0"+num_minas);

		else $("#minas").text(num_minas);
	}

}

//Para y reinicia el tiempo y vuelve a iniciar de nueva la partida
function reiniciarPartida(){

	if(primerClick){
  		clearInterval(intervalSegundos);
  		clearInterval(intervalMinutos);
  	}

  	$("#tiempo").text("00:00");

  	iniciarPartida();

}

//Inicia el tiempo
function iniciarMarcadorTiempo(){
	intervalSegundos = setInterval(function(){
		if(!gameStoped){
			if(minutos<10){
				if(segundos<10){
					$("#tiempo").text("0"+ minutos + ":0" + segundos);
				}else{
					$("#tiempo").text("0" + minutos + ":" + segundos);
				}
			}else{
				if(segundos<10){
					$("#tiempo").text(minutos + ":0" + segundos);
				}else{
					$("#tiempo").text(minutos + ":" + segundos);
				}
			} 
		}

		segundos++;
	}, 1000);

	intervalMinutos = setInterval(function(){
		segundos=0;
		minutos++;
		
		//si llega al minuto 60 se pierde la partida
		if(minutos>=60){
			gameOver(-1,-1);
			voltearCasillas();
		}

	}, 60*1000);
}

function victoria(){
	
	//Espero a que se desvanezca el header primero
  	setTimeout(function(){
  		if(win){
  			//La separacion de la "a" del resto de la palabra es debido a que el letter-spacing
  			//genera un espacio de mas a la derecha, asi ocasionando que no se pueda centrar
  			$("header>h1").html("<span>Victori</span>a");
  		}
  	}, 1000*1);
  	
  	$("header").attr("class", "victoria");

	win=true;
  	expresionEmoticono("win");
	gameOver(-1,-1);
}

function derrota(valI, valX){
	
	//Espero a que se desvanezca el header primero
  	setTimeout(function(){
  		if(gameStoped){
  			//La separacion de la "a" del resto de la palabra es debido a que el letter-spacing
	  		//genera un espacio de mas a la derecha, asi ocasionando que no se pueda centrar
	  		$("header>h1").html("<span>Derrot</span>a");
  		}
  	}, 1000*1);
  	$("header").attr("class", "derrota");

	$("#i"+valI+"x"+valX).css("background", "#B73C3C");
  	expresionEmoticono("lost");
  	voltearCasillas();
}

function expresionEmoticono(expresion){

	switch(expresion){
		case "happy":
			$("#nueva_partida").html("<img src='images/happy.png' class='emoticono'>");
			break;
		case "lost":
			$("#nueva_partida").html("<img src='images/lost.png' class='emoticono'>");
			break;
		case "win":
			$("#nueva_partida").html("<img src='images/winner.png' class='emoticono'>");
			break;
		default:
			$("#nueva_partida").html("<img src='images/happy.png' class='emoticono'>");
	}

}

function nivelDificultad(dificultad){
	switch(dificultad){
  		case 1:
  			minas=10;
		  	filas = 9;
		  	columnas = 9;
		  	break;
		case 2: 
			minas=30;
		  	filas = 12;
		  	columnas = 12;
		  	break;
		case 3: 
			minas=89;
		  	filas = 15;
		  	columnas = 25;
		  	break;
		case 4:
			minas=100;
		  	filas = 20;
		  	columnas = 20; 
		  	break;
		default:

  	}
}

//Setea los colores de las casillas segun el numero 
function setColor(id){

  	switch($(id).text()){
		case "1":
			$(id).css("color", "#24DC00");
			break;
		case "2":
			$(id).css("color", "#00A8BF");
			break;
		case "3":
			$(id).css("color", "#7C06C8");
			break;
		case "4":
			$(id).css("color", "#DDC200");
			break;
		case "5":
			$(id).css("color", "#C65D00");
			break;
		case "6":
			$(id).css("color", "#008C20");
			break;
		case "7":
			$(id).css("color", "#20008C");
			break;
		case "8":
			$(id).css("color", "#E60000");
			break;
	}	

}

function getRandom(min, max){
	return Math.round(Math.random() * (max - min)) + min;
}


	
